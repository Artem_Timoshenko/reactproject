import React, {Component} from 'react';
import './subtask-list.component.css';

class SubTaskListComponent extends Component {

  constructor() {
    super();
  }

  render() {
    return (
      <div className="Sub-task-list-container">
        <div>
          <input placeholder="Enter subtask title" />
          <button>Add</button>
        </div>
        <div className="Sub-task-list">

        </div>
      </div>
    );
  }
}

export default SubTaskListComponent;
