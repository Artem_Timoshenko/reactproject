import React, {Component} from 'react';
import {TaskListComponent} from './task-list';
import {SubTaskListComponent} from './subtask-list'
import {TodoHeadComponent} from './todo-head'
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state;
  newName;
  listStyle = {};

  constructor() {
    super();
    this.state = {data: ['item1', 'item2', 'item3']};
  }

  userList(props) {}

  handleClick(name) {
    const names = this.state.data;
    // names.push(name);
    // this.setState({data: names});
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
        </div>
        <div className="App-layout">
          <div className="Todo-header">
            <TodoHeadComponent />
          </div>
          <TaskListComponent />
          <SubTaskListComponent />

          {/*<p className="App-intro">*/}
          {/*{this.data.map((elem) => <li><button onClick={this.handleClick}>{elem}</button></li>)}*/}
          {/*</p>*/}
          {/*<p>*/}
            {/*{this.state.data.map((elem) => <li>{elem}</li>)}*/}
          {/*</p>*/}
          {/*<input>{this.newName}</input>*/}
          {/*<button onClick={this.handleClick(this.newName)}>add name</button>*/}
        </div>
      </div>
    );
  }
}

export default App;
