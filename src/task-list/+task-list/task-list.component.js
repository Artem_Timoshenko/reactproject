import React, {Component} from 'react';
// import {TaskItemComponent} from '../task-item'
import {TaskModel, TaskListModel} from '../models'
import './task-list.component.css';

class TaskListComponent extends Component {

  data = ['task1', 'task2'];

  tasks = new TaskListModel(this.data);

  constructor() {
    super();
    // this.addTask();
  }

  addTask() {
    console.log(this.tasks);
    this.tasks.createTask('test');
    console.log(this.tasks);
  }

  render() {
    return (
      <div className="Task-list-container">
        <div>
          <input placeholder="Enter category title" />
          <button onClick={this.addTask}>Add</button>
        </div>
        <div className="Task-list">
          <ul>{this.tasks.getTasksList().map((task) => {
            <li>{task}</li>
          })}</ul>
          {/*<TaskItemComponent />*/}
        </div>
      </div>
    );
  }
}

export default TaskListComponent;
