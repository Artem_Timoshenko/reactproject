import {TaskModel} from './task.model'

export class TaskListModel {
  _data = new Map();

  constructor(data) {
    for (const task of data) {
      this._data.set(task, new TaskModel(name));
    }
  }

  // getTask(name) {
  //   return this._data.get(name);
  // }

  getTasksList() {
    return ([...this._data.values()]);
  }

  createTask(name) {
    this._data.set(name, new TaskModel(name));
  }

  // createSubTask(name) {
  //
  // }
}
