import React, {Component} from 'react';
import './todo-head.component.css';

class TodoHeadComponent extends Component {

  constructor() {
    super();
  }

  render() {
    return (
      <div className="Todo-head-container">
        <div>
          <span className="head-text">To-Do List</span>
        </div>
        <div className="filter">
          <input type="checkbox"/>
          <span>show active</span>
          <input/>
        </div>
      </div>
    );
  }
}

export default TodoHeadComponent;
